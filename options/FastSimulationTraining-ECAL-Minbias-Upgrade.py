# general options for Gauss needed to produce a minbias sample

from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-Upgrade-Baseline.py")
importOptions("$DECFILESROOT/options/30000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")

# use the upgrade geometry with the latest modification
# that takes into account the tilt in neutron shielding
from Configurables import LHCbApp
LHCbApp().DDDBtag = "upgrade/dddb-20210617"
LHCbApp().CondDBtag = "upgrade/sim-20210617-vc-mu100"
LHCbApp().Simulation = True
LHCbApp().EvtMax = 50

# we are going to place an external plane in front of the zMax plane
# which for the upgrade is set to 11948. mm and tilted by .207 deg

from GaudiKernel.SystemOfUnits import m, mm, degree
zmax = 11948. * mm
collector_z_size = .01 * mm
collector_x_size = 10. * m
collector_y_size = 10. * m
collector_z_pos = zmax - collector_z_size / 2.
collector_angle = .207 * degree

# turn the ExternalDetectorEmbedder on, it is a tool
# that will insert an external plane in front of the calorimeter
from Configurables import ExternalDetectorEmbedder
external = ExternalDetectorEmbedder("EcalCollectorEmbedder")

# generating ecal collector plane
external.Shapes = {
    'EcalCollector': {
        "Type": "Cuboid",
        "zPos": collector_z_pos,
        "xSize": collector_x_size,
        "ySize": collector_y_size,
        "zSize": collector_z_size,
        "xAngle": -collector_angle,
    }
}

external.Sensitive = {
    'EcalCollector': {
        "Type": "MCCollectorSensDet",
        "RequireEDep": False,
        "OnlyForward": True,
        "OnlyAtBoundary": True,
    },
}

external.Hit = {
    'EcalCollector': {
        'Type': 'GetMCCollectorHitsAlg',
    },
}

external.Moni = {
    'EcalCollector': {
        'Type': 'EcalCollector',
        'CheckParent': True,
        'CollectorZ': collector_z_pos,
        'CollectorXAngle': collector_angle,
    },
}

# since the collector plane will overlap with other
# volumes, it has to be placed in a parallel world
from Configurables import ParallelGeometry, Gauss
Gauss().ParallelGeometry = True
ParallelGeometry().ParallelWorlds = {
    'EcalCollectorParallelWorld': {
        'ExternalDetectorEmbedder': 'EcalCollectorEmbedder',
    },
}

ParallelGeometry().ParallelPhysics = {
    'EcalCollectorParallelWorld': {
        'LayeredMass': False,
    }
}

# store the tuple in FastSimulationTraining-ECAL-Minbias-Upgrade.root
from Configurables import ApplicationMgr
ApplicationMgr().ExtSvc += ["NTupleSvc"]
from Configurables import NTupleSvc
NTupleSvc().Output = [
    "FILE1 DATAFILE='FastSimulationTraining-ECAL-Minbias-Upgrade.root' TYP='ROOT' OPT='NEW'"
]
