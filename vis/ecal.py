beamAngle = 3.601e-3
EcalYShift = -0.19
EcalStart = 12520.0
EcalZSize = 825.0
EcalPosXY = EcalStart + EcalZSize / 2.0
EcalInnModLength = 690.0
EcalMidModLength = 705.0
EcalOutModLength = 705.0
EcalInnFrontCoverLength = 23.0
EcalStackLength = 432.0
EcalMidFrontCoverLength = 23.0
EcalOutFrontCoverLength = 23.0
EcalOutStackOffset = -0.5 * EcalOutModLength + EcalOutFrontCoverLength + 0.5 * EcalStackLength
EcalInnStackOffset = -0.5 * EcalInnModLength + EcalInnFrontCoverLength + 0.5 * EcalStackLength
EcalMidStackOffset = -0.5 * EcalMidModLength + EcalMidFrontCoverLength + 0.5 * EcalStackLength

EcalInnOffset = 7.5
EcalMidOffset = 0.0
EcalOutOffset = 0.0

EcalInnStackStart = EcalPosXY + EcalInnOffset + EcalInnStackOffset - 0.5 * EcalStackLength
EcalMidStackStart = EcalPosXY + EcalMidOffset + EcalMidStackOffset - 0.5 * EcalStackLength
EcalOutStackStart = EcalPosXY + EcalOutOffset + EcalOutStackOffset - 0.5 * EcalStackLength

EcalSteelOffset = -215.50
EcalPlasticOffset = -211.5
EcalScOffset = -205.88
EcalPbOffset = -202.76
EcalOffset = -215.50

EcalSteelThick = 1.0
EcalPlasticThick = 7.0
EcalPbThick = 2.0
EcalScThick = 4.0
EcalPaperThick = 0.12
EcalSandwichThick = EcalScThick + EcalPbThick + 2 * EcalPaperThick

# EcalInnSteelStart = EcalInnStackStart + EcalStackLength / 2.0 + EcalSteelOffset - EcalSteelThick / 2.0
# EcalPlasticStart = EcalInnStackStart + EcalStackLength / 2.0 + EcalPlasticOffset - EcalPlasticThick / 2.0
# EcalPbStart = EcalInnStackStart + EcalStackLength / 2.0 + EcalPbOffset - EcalPbThick / 2.0
EcalScStart = {
    'inner': EcalInnStackStart + EcalStackLength / 2.0 + EcalScOffset - EcalScThick / 2.0,
    'middle': EcalMidStackStart + EcalStackLength / 2.0 + EcalScOffset - EcalScThick / 2.0,
    'outer': EcalOutStackStart + EcalStackLength / 2.0 + EcalScOffset - EcalScThick / 2.0,
}
EcalModXYSize = 121.9
BOUNDARIES = {'middle': {'x': {'min': -32 * EcalModXYSize / 2.0, 'max': 32 * EcalModXYSize / 2.0},
                         'y': {'min': -20 * EcalModXYSize / 2.0, 'max': 20 * EcalModXYSize / 2.0},
                         'z': {'min': EcalMidStackStart, 'max': EcalMidStackStart + EcalStackLength}},
              'inner': {'x': {'min': -16 * EcalModXYSize / 2.0, 'max': 16 * EcalModXYSize / 2.0},
                        'y': {'min': -12 * EcalModXYSize / 2.0, 'max': 12 * EcalModXYSize / 2.0},
                        'z': {'min': EcalInnStackStart, 'max': EcalInnStackStart + EcalStackLength }},
              'beam': {'x': {'min': -6 * EcalModXYSize / 2.0 + EcalModXYSize / 3.0, 'max': 6 * EcalModXYSize / 2.0 - EcalModXYSize / 3.0},
                       'y': {'min': -4 * EcalModXYSize / 2.0, 'max': 4 * EcalModXYSize / 2.0}},
              'outer': {'x': {'min': -64 * EcalModXYSize / 2.0, 'max': 64 * EcalModXYSize / 2.0},
                        'y': {'min': -52 * EcalModXYSize / 2.0, 'max': 52 * EcalModXYSize / 2.0},
                        'z': {'min': EcalOutStackStart, 'max': EcalOutStackStart + EcalStackLength}},
              }

UNIT_CELL_SIZE = EcalModXYSize / 6.0

