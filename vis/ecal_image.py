from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import math
from vis.ecal import BOUNDARIES, UNIT_CELL_SIZE, EcalModXYSize, UNIT_CELL_SIZE

class EcalImage():

    @staticmethod
    def prepare_dimension_columns(dimension, val):
        dim_cols = []
        for i in range(val*6):
            dim_cols.append(BOUNDARIES['outer'][dimension]['min'] + i * UNIT_CELL_SIZE)
        return dim_cols

    @staticmethod
    def to_pxl(cell, axis):
        return (cell - BOUNDARIES['outer'][axis]['min']) / (BOUNDARIES['outer'][axis]['max'] - BOUNDARIES['outer'][axis]['min'])

    def fill_cells_with_energy(self):
        # FIXME: don't like this zipping of colums, but it seems to be the fastest for now
        for cell_x, cell_y, cell_size, energy in zip(
            self.hits_pickle['Cell_X'],
            self.hits_pickle['Cell_Y'],
            self.hits_pickle['Cell_Size'],
            self.hits_pickle['Active_Energy']):

            pxl_x = round(self.to_pxl(cell_x - cell_size / 2., 'x') * 384.)
            pxl_y = round(self.to_pxl(cell_y - cell_size / 2., 'y') * 312.)
            pxl_size = int(cell_size / EcalModXYSize * 6)
            self.E[pxl_y : pxl_y + pxl_size, pxl_x : pxl_x + pxl_size] += energy

    def __init__(self, hits_pickle, ):
        self.hits_pickle = hits_pickle
        if isinstance(hits_pickle, str):
            self.hits_pickle = pd.read_pickle(hits_pickle)
        x = self.prepare_dimension_columns('x', 64)
        y = self.prepare_dimension_columns('y', 52)
        self.X, self.Y = np.meshgrid(x, y)
        self.E = np.zeros((len(y), len(x)))
        self.fill_cells_with_energy()
        self.E[self.E <= 0.0] = np.NaN

    def plot_links(self, ax, df, **kwargs):
        tdf = df[['Particle_Index', 'Active_Energy']].groupby(by="Particle_Index").max().copy()
        tdf = df.merge(tdf, left_on='Particle_Index', right_on='Particle_Index', suffixes=('', '_Max'))
        tdf = tdf[tdf['Active_Energy'] == tdf['Active_Energy_Max']]
        for x_part, y_part, x_cell, y_cell in zip(tdf['Entry_X'],
                                                  tdf['Entry_Y'],
                                                  tdf['Cell_X'],
                                                  tdf['Cell_Y']):

            ax.plot([x_part, x_cell],
                    [y_part, y_cell],
                    **kwargs)

    def plot_points(self, ax, df, position_type='', size=20, **kwargs):
        for x_pos, y_pos in zip(df['Position X' + position_type], df['Position Y' + position_type]):
            point = plt.Circle((x_pos, y_pos), size, **kwargs)
            ax.add_artist(point)

    def plot_grid(self, ax, cmap=plt.get_cmap('viridis'), module_boundary_color='grey', rasterized=False, nan_color=None):
        ax.set_xlim(BOUNDARIES['outer']['x']['min'], BOUNDARIES['outer']['x']['max'])
        ax.set_ylim(BOUNDARIES['outer']['y']['min'], BOUNDARIES['outer']['y']['max'])
        for module in ['middle', 'inner', 'beam']:
            ax.add_artist(plt.Rectangle((BOUNDARIES[module]['x']['min'], BOUNDARIES[module]['y']['min']),
                                         height=(BOUNDARIES[module]['y']['max'] - BOUNDARIES[module]['y']['min']),
                                         width=(BOUNDARIES[module]['x']['max'] - BOUNDARIES[module]['x']['min']),
                                         color=module_boundary_color, linewidth=1, fill=False))
        if nan_color:
            cmap.set_bad(nan_color, 1.)
        return ax.pcolormesh(self.X, self.Y, self.E, cmap=cmap, rasterized=rasterized)
