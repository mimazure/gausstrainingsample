from collections import defaultdict
from vis.ecal import EcalScStart, EcalPosXY, EcalYShift, beamAngle
import math
import numpy as np
import pandas as pd

class LHCbImage():

    @staticmethod
    def rotate(origin, point, angle):
        ox, oy = origin
        px, py = point
        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        return qx, qy

    @staticmethod
    def rotate_lhcb_plane(z_point=EcalScStart['outer'], y_points=[-7500, 7500]):
        z_rot1, y_rot1 = LHCbImage.rotate([EcalPosXY, 0.], [float(z_point), y_points[0] - EcalYShift], -beamAngle)
        z_rot2, y_rot2 = LHCbImage.rotate([EcalPosXY, 0.], [float(z_point), y_points[1] - EcalYShift], -beamAngle)
        return [z_rot1, z_rot2], [y_rot1, y_rot2]

    @staticmethod
    def prepare_dimension_columns(vals, val_min, val_max):
        dim_cols = []
        for val in range(vals):
            dim_cols.append(val_min + (val ) * (val_max - val_min) / vals)
        return dim_cols

    def __init__(self, vals, y_min, y_max, z_min, z_max):
        x = self.prepare_dimension_columns(vals, z_min, z_max)
        y = self.prepare_dimension_columns(vals, y_min, y_max)
        self.X, self.Y = np.meshgrid(x, y)
        self.E = np.zeros((len(y), len(x)))
        thickness = defaultdict(float)

    def plot_grid(self, ax, pickle, **kwargs):
        df = pd.read_pickle(pickle)
        for row_x, row_y, density in zip(df['z'],
                                         df['y'],
                                         df['density']):
            self.E[row_y, row_x] += density #* thick\n",

        self.E[self.E <= 0.0] = np.NaN
        return ax.pcolormesh(self.X, self.Y, self.E, **kwargs)

    def plot_links(self, ax, df, **kwargs):

        tdf = df[['Particle_Index', 'Active_Energy']].groupby(by="Particle_Index").max().copy()
        tdf = df.merge(tdf, left_on='Particle_Index', right_on='Particle_Index', suffixes=('', '_Max'))
        tdf = tdf[tdf['Active_Energy'] == tdf['Active_Energy_Max']]
        for x_part, y_part, x_cell, y_cell in zip(tdf['Entry_Z'],
                                                  tdf['Entry_Y'],
                                                  tdf['Cell_Z'],
                                                  tdf['Cell_Y']):
            ax.plot([x_part, x_cell],
                    [y_part, y_cell],
                    **kwargs)
